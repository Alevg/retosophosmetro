package pe.metro.www.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

//Clase donde se encuentran los xpath de los botones y campos que se mapean.
public class ProductoPage {

    //Para el xpath TXT_PRODUCTO, es dinámico {0}, para recibir el parámetro desde el feature, y se implementa en los tasks como target.of(variable-producto)
    public static final Target TXT_PRODUCTO = Target.the("producto").locatedBy("//a[contains(text(),'{0}')]");
    public static final Target BTN_AGREGAR_CARRITO = Target.the("botón agregar al carrito").locatedBy("//span[text()='Agregar al carrito']");
    public static final Target BTN_CARRITO = Target.the("botón carrito").locatedBy("//button[@class='btn red minicart__action--toggle-open food-site']");
    public static final Target BTN_VER_CARRITO =Target.the("botón ver carrito").locatedBy("//span[contains(text(),'Ver carrito')]");
    public static final Target TXT_BUSCAR = Target.the("buscador").locatedBy("//input[@id='search-autocomplete-input']");
    public static final Target BTN_NO_GRACIAS = Target.the("Botón notificaciones").locatedBy("//button[text()='no gracias']");
}
