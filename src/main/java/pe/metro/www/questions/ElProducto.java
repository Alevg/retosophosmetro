package pe.metro.www.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static pe.metro.www.userinterfaces.ProductoPage.TXT_PRODUCTO;

public class ElProducto implements Question<Boolean> {

    private String producto;

    public ElProducto(String producto) {
        this.producto = producto;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        //retorna verdadero, si el target del producto es visible
        return TXT_PRODUCTO.of(producto).resolveFor(actor).isVisible();
    }

    public static ElProducto seAgregoCorrectamente(String producto) {
        return new ElProducto(producto);
    }
}