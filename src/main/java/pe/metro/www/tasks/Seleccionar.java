package pe.metro.www.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import pe.metro.www.models.DatosProductos;

import java.util.List;

import static pe.metro.www.userinterfaces.ProductoPage.*;

public class Seleccionar implements Task {

    private String producto;

    //Constructor donde recibe el prametro desde el modelo
    public Seleccionar(List<DatosProductos> datosProductos) {
        this.producto = datosProductos.get(0).getProducto();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
            /*Se realizan las diferentes acciones sobre los targets.
        1. Cierra el mensaje de notificación
        2. Busca el producto
        3. Click el producto
        4. Lo agrega al carrito
        5. Va al carrito
         */
        CerrarMensajeNotificacion(actor);
        RealizarProcesoCompra(actor);
    }
   //Se realiza abstracción de metodos por cada acción
    private <T extends Actor> void RealizarProcesoCompra(T actor) {
        actor.attemptsTo(Enter.theValue(producto).into(TXT_BUSCAR),
                Click.on(TXT_PRODUCTO.of(producto)),
                Click.on(BTN_AGREGAR_CARRITO),
                Click.on(BTN_CARRITO),
                Click.on(BTN_VER_CARRITO));
    }
    //Se realiza abstracción de metodos por cada acción
    private <T extends Actor> void CerrarMensajeNotificacion(T actor) {
    //Se valida si el target del mensaje de notificación esta activo y luego se da click en el botón no gracias, para continuar con la busqueda
      BTN_NO_GRACIAS.resolveFor(actor).waitUntilVisible();
        if (BTN_NO_GRACIAS.resolveFor(actor).isPresent()) {
            actor.attemptsTo(Click.on(BTN_NO_GRACIAS));
        }
    }

    //Retorna la instancia de la clase
    public static Seleccionar elProducto(List<DatosProductos> datosProductos) {
        return Tasks.instrumented(Seleccionar.class, datosProductos);
    }
}
