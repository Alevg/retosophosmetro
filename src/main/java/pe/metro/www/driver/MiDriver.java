package pe.metro.www.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MiDriver {

    static WebDriver driver;

    //clase donde se crea el propio web driver.
    public static MiDriver web() {
        //hace referencia a las propiedades y ruta donde se encuentra el chrome driver
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        //para m�ximizar la ventana
        driver.manage().window().maximize();
        return new MiDriver();
    }

    public WebDriver enLaPagina(String url) {
        driver.get(url);
        return driver;
    }
}
