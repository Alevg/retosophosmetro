#Author: Alejandra Villa Giraldo

Feature: Yo como usuario de Metro, quiero realizar el proceso de compra de un producto.

  Scenario:Proceso de compra de un producto en Metro
    Given ingreso a la pagina de Metro cencosud
    When  selecciono el producto
      | producto                |
      | Pan Pita Integral Bimbo |
    Then valido que el producto Pan Pita Integral Bimbo fue agregado exitosamente