package pe.metro.www.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/pe.metro.www.features/comprar_producto.feature",
        glue = "pe.metro.www.stepdefinitions", snippets = SnippetType.CAMELCASE)
public class ComprarProducto {
}
