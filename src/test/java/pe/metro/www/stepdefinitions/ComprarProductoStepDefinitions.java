package pe.metro.www.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import pe.metro.www.driver.MiDriver;
import pe.metro.www.models.DatosProductos;
import pe.metro.www.questions.ElProducto;
import pe.metro.www.tasks.Seleccionar;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class ComprarProductoStepDefinitions {

    Actor actor;
   //En esta clase, se colocan por medio de metodos los pasos en el feature, esto con el fin de comenzar con la capa de implementación

    @Given("^ingreso a la pagina de Metro cencosud$")
    public void ingreso_a_la_pagina_de_Metro_cencosud() {
        //Se coloca  por medio de metodos la funcionalidad que realiza el actor
        actor = Actor.named("Alejandra");
        //el actor realiza la tarea de ingresar a la pagina web indicada.
        actor.can(BrowseTheWeb.with(MiDriver.web().enLaPagina("https://www.metro.pe/especiales/cybermetro")));
    }


    @When("^selecciono el producto$")
    public void selecciono_el_producto(List<DatosProductos> datosProductos) {
        /*Metodo donde se va a implementar la busqueda del producto, se envía una Lista de DatoProducto como parametro
        que recibe la tarea */
        actor.attemptsTo(Seleccionar.elProducto(datosProductos));
    }

    @Then("^valido que el producto (.*) fue agregado exitosamente$")
    public void valido_que_el_producto_fue_agregado_exitosamente(String producto) {
        /*Metodo donde se implementa la validación que el producto exista en el carrito*/
        actor.should(seeThat(ElProducto.seAgregoCorrectamente(producto)));
    }
}
