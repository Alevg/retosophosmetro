# RETO AUTOMATIZACIÓN SOPHOS SOLUTION 

**Escenario de prueba:** Proceso de compra de un producto y validación en el carrito de compras para la página web www.metro.pe

_Consta de:_

_**Runner:** Clase donde se tiene alojada de ruta del feature y step definitions._

_**Step Definitios:** Clase donde se encuentran los metodos generados del feature, para la implementación a nivel de código de los casos de prueba._
                  _Es la clase intermedia entre el feature y los tasks._
                  
_**Driver:** Clase donde se encuentra la implementación del propio driver, utilizando el Chrome Driver_

_**Models:** Clase model donde se crean los get de las variables de una Lista, que se van a utilizar desde el feature hasta los tasks._
         _En este caso se tiene la variable producto, que se envío desde el Feature como una Lista._
         _Desde el task, será llamada la clase model con un get, para obtener el valor de la variable._
         
_**Question:** Clase que retorna un Boleano, el cual valida si el target del producto es visible._

_**Task:** Clase donde se implementan las acciones del actor donde interactua con los target, hace click en el producto, luego click_
          _en agregar el carrito y luego click en el carrito._
          
_**Feaute:** Archivo donde se encuentra en lenguaje gherkin, lenguaje de negocio el escenario de prueba con los diferentes pasos._
         _Ingreso a la pagina de metro (Given) o precondición, Selecciono el producto (When) y Valido que el producto se agrego al carrito (Then, el cual esta asociado al question)._
         _Desde el feature se envían las variables, en este caso el producto._
         _En el When la variable esta como lista y en el Then esta como String._
         
_**User Interface:** Clase donde se encuentran todos los target como xpath._

### Ejecución de pruebas:

_Se tiene el **chromedriver.exe** en la raiz del proyecto, compatible con versión de Chrome 78._

_Para obtener las evidencias de Serenity y correr exitosamente la prueba se ejecuta el comando:_

_gradle clean test --tests *ComprarProducto* aggregate --info_

### Construido con:
_Patrón de diseño **Screenplay.**_

_**Gradle**_

_Control de versión **Git**_

_Lenguaje de programación **Java**._

_Automatización **Serenity BDD.**_

-Framework **IntelliJ**

## Realizado por: Alejandra Villa Giraldo.

